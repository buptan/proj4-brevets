from acp_times import *

def test_open_time():
    now = arrow.now()
    
    test1 = now.shift(hours=+round((60/34)//1), minutes=+round(((60/34)%1)*60))
    assert open_time(60, 200, now) == test1.isoformat()

    test2 = now.shift(hours=+round((120/34)//1), minutes=+round(((120/34)%1)*60))
    assert open_time(120, 200, now) == test2.isoformat()

    test3 = now.shift(hours=+round((175/34)//1), minutes=+round(((175/34)%1)*60))
    assert open_time(175, 200, now) == test3.isoformat()

    test4 = now.shift(hours=+round((205/34)//1), minutes=+round(((205/34)%1)*60))
    assert open_time(205, 200, now) == test4.isoformat()

    dh = round((200/34)//1 + (150/32)//1)
    dm = round(((200/34)%1)*60 + ((150/32)%1)*60)
    test5 = now.shift(hours=+dh, minutes=+dm)
    assert open_time(350, 600, now) == test5.isoformat()

    tot = (200/34) + (200/32) + (205/30)
    dh = round(tot//1)
    dm = round((tot%1)*60)
    test6 = now.shift(hours=+dh, minutes=+dm)
    assert open_time(605, 600, now) == test6.isoformat()

    dh = round((200/34)//1 + (200/32)//1 + (200/30)//1 + (290/28)//1)
    dm = round(((200/34)%1)*60 + ((200/32)%1)*60 + ((200/30)%1)*60 + ((290/28)%1)*60)
    test7 = now.shift(hours=+dh, minutes=+dm)
    assert open_time(890, 1000, now) == test7.isoformat()


def test_close_time():
    now = arrow.now()
    
    test1 = now.shift(hours=+round((60/15)//1), minutes=+round(((60/15)%1)*60))
    print("Test = {}".format(test1))
    assert close_time(60, 200, now) == test1.isoformat()

    test2 = now.shift(hours=+round((120/15)//1), minutes=+round(((120/15)%1)*60))
    assert close_time(120, 200, now) == test2.isoformat()

    test3 = now.shift(hours=+round((175/15)//1), minutes=+round(((175/15)%1)*60))
    assert close_time(175, 200, now) == test3.isoformat()

    test4 = now.shift(hours=+round((205/15)//1), minutes=+round(((205/15)%1)*60))
    assert close_time(205, 200, now) == test4.isoformat()

    dh = round((200/15)//1 + (150/15)//1)
    dm = round(((200/15)%1)*60 + ((150/15)%1)*60)
    test5 = now.shift(hours=+dh, minutes=+dm)
    assert close_time(350, 600, now) == test5.isoformat()

    tot = (200/15) + (200/15) + (205/15)
    dh = round(tot//1)
    dm = round((tot%1)*60)
    test6 = now.shift(hours=+dh, minutes=+dm)
    assert close_time(605, 600, now) == test6.isoformat()

    dh = round((200/15)//1 + (200/15)//1 + (200/15)//1 + (290/11.428)//1)
    dm = round(((200/15)%1)*60 + ((200/15)%1)*60 + ((200/15)%1)*60 + ((290/11.428)%1)*60)
    test7 = now.shift(hours=+dh, minutes=+dm)
    assert close_time(890, 1000, now) == test7.isoformat()
