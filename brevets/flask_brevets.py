"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet_cat = request.args.get('brevet_dist', type=float)
    begin_date = request.args.get('begin_date')
    begin_time = request.args.get('begin_time')
    #print("Begin Date = {}\nBegin Time = {}".format(begin_date, begin_time))
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    add = [0,0,0]

    sect = 0
    ct = 0
    for char in begin_date:
        if char == '-':
            if ct == 1:
                add[sect] = 1
            ct = 0
            sect = sect + 1
        else:
            ct = ct + 1
    d_string = ""
    sect = 0
    for char in begin_date:
        if char == '-':
            sect = sect + 1
        elif add[sect] == 1:
            d_string = d_string + '0'
        d_string = d_string + char

    use_time = arrow.get(d_string + ' ' + begin_time, 'DD-MM-YYYY HH:mm')
    use_time.replace(tzinfo='US/Pacific')
    #use_time.to('US/Pacific')

    open_time = acp_times.open_time(km, brevet_cat, use_time)
    close_time = acp_times.close_time(km, brevet_cat, use_time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
