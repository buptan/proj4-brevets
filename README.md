# Project 4: Brevet time calculator with Ajax

## acp_times.py
This program is the logic to calculate open and close times for control points for randonneurs. There are 
different speeds for different distance categories. For 0-200km, max speed is 34, min speed is 15. For 200-400km,
max speed is 32, min speed is 15. For 400-600km, max speed is 30, min speed is 15. For 600-1000km, max speed is 
28, min speed is 11.428. For 1000-1300km, max speed is 26, min speed is 13.333. To calculate the open and close 
times of a given control point, the user will specify the brevet category and the distance to the control point, 
for open time we divide the control point distance by the max speed of each category up until that distance. For 
instance, to calculate open time for a control point at 890km in a 1000km brevet, it would be 200/34 + 200/32 + 
200/30 + 290/28. To calculate the close time it is essentially the same, but with the min speed for each distance 
category. In the previous example that would be 200/15 + 200/15 + 200/15 + 290/11.428.

If a control point is slightly over the brevet category, over by 10 or less, it is still calculated in that brevet
category. For example, consider a control point at 205km in a 200km brevet category. It is still calculated with 
the 0-200km max and min speeds, so open time is 200/34, close time is 200/15. However, a control point at 211km 
would be calculated using the 0-200km for the first 200km, then 200-400km category for the last 11km, but in this 
case the user should not select 200km as the brevet category.
